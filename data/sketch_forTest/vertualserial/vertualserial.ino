void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(3,OUTPUT);
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  pinMode(6,INPUT);
  pinMode(7,INPUT);
  pinMode(8,INPUT);
  digitalWrite(3,HIGH);
}

void loop() {

  // pin4がHIGHなら、5cmデータ
  if(digitalRead(4)==HIGH){
    Serial.print("Receive Data(");
    Serial.print("1F400000\t12345678)\n");    
  }
  // pin5がHIGHなら、10cmデータ
  else if(digitalRead(5)==HIGH){
    Serial.print("Receive Data(");
    Serial.print("3E800000\t12345678)\n");
  }
  // pin6がHIGHなら、テストデータ(0cm)
  else if(digitalRead(6)==HIGH){
    Serial.print("Receive Data(");
    Serial.print("00000011\t12345678)\n");
  }
  // pin7がHIGHなら、テストデータ(7.5cm)
  else if(digitalRead(7)==HIGH){
    Serial.print("Receive Data(");
    Serial.print("2EE00000\t12345678)\n");
  }
  // pin8がHIGHなら、テストデータ(15cm)
  else if(digitalRead(8)==HIGH){
    Serial.print("Receive Data(");
    Serial.print("5DC00000\t12345678)\n");
  }
  // pin8がHIGHなら、テストデータ(15cm)
  // 全部の入力ピンがLOWなら、テストデータ
  else{
    Serial.print("Receive Data(");
    Serial.print("5DC00000\t12345678)\n");
  }
    delay(1000);    
  
}
