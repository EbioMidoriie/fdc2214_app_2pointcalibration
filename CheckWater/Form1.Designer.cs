﻿namespace CheckWater
{
    partial class fm_main
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm_main));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.group_water = new System.Windows.Forms.GroupBox();
            this.label_water = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.group_calib = new System.Windows.Forms.GroupBox();
            this.bt_calreset = new System.Windows.Forms.Button();
            this.bt_10cm = new System.Windows.Forms.Button();
            this.bt_5cm = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bt_connect = new System.Windows.Forms.Button();
            this.cb_com = new System.Windows.Forms.ComboBox();
            this.timGetData = new System.Windows.Forms.Timer(this.components);
            this.groupBox_debug = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_save_cap_10cm = new System.Windows.Forms.Label();
            this.lb_save_cap_5cm = new System.Windows.Forms.Label();
            this.lb_save_delta = new System.Windows.Forms.Label();
            this.lb_save_offset = new System.Windows.Forms.Label();
            this.label_CH0value = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.group_water.SuspendLayout();
            this.group_calib.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox_debug.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox_debug);
            this.groupBox1.Controls.Add(this.group_water);
            this.groupBox1.Controls.Add(this.group_calib);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.bt_connect);
            this.groupBox1.Controls.Add(this.cb_com);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(559, 482);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "操作手順";
            // 
            // group_water
            // 
            this.group_water.Controls.Add(this.label_water);
            this.group_water.Controls.Add(this.label1);
            this.group_water.Location = new System.Drawing.Point(305, 145);
            this.group_water.Name = "group_water";
            this.group_water.Size = new System.Drawing.Size(247, 107);
            this.group_water.TabIndex = 4;
            this.group_water.TabStop = false;
            this.group_water.Text = "現在の水位";
            // 
            // label_water
            // 
            this.label_water.AutoSize = true;
            this.label_water.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_water.Location = new System.Drawing.Point(52, 47);
            this.label_water.Name = "label_water";
            this.label_water.Size = new System.Drawing.Size(37, 37);
            this.label_water.TabIndex = 1;
            this.label_water.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(174, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "cm";
            // 
            // group_calib
            // 
            this.group_calib.Controls.Add(this.bt_calreset);
            this.group_calib.Controls.Add(this.bt_10cm);
            this.group_calib.Controls.Add(this.bt_5cm);
            this.group_calib.Location = new System.Drawing.Point(306, 261);
            this.group_calib.Name = "group_calib";
            this.group_calib.Size = new System.Drawing.Size(247, 201);
            this.group_calib.TabIndex = 3;
            this.group_calib.TabStop = false;
            this.group_calib.Text = "キャリブレーション";
            // 
            // bt_calreset
            // 
            this.bt_calreset.Location = new System.Drawing.Point(34, 172);
            this.bt_calreset.Name = "bt_calreset";
            this.bt_calreset.Size = new System.Drawing.Size(176, 23);
            this.bt_calreset.TabIndex = 2;
            this.bt_calreset.Text = "キャリブレーション情報をリセット";
            this.bt_calreset.UseVisualStyleBackColor = true;
            this.bt_calreset.Click += new System.EventHandler(this.bt_calreset_Click);
            // 
            // bt_10cm
            // 
            this.bt_10cm.Location = new System.Drawing.Point(34, 39);
            this.bt_10cm.Name = "bt_10cm";
            this.bt_10cm.Size = new System.Drawing.Size(176, 43);
            this.bt_10cm.TabIndex = 1;
            this.bt_10cm.Text = "10cm水位を補正";
            this.bt_10cm.UseVisualStyleBackColor = true;
            this.bt_10cm.Click += new System.EventHandler(this.bt_10cm_Click);
            // 
            // bt_5cm
            // 
            this.bt_5cm.Location = new System.Drawing.Point(34, 99);
            this.bt_5cm.Name = "bt_5cm";
            this.bt_5cm.Size = new System.Drawing.Size(176, 43);
            this.bt_5cm.TabIndex = 0;
            this.bt_5cm.Text = "5cm水位を補正";
            this.bt_5cm.UseVisualStyleBackColor = true;
            this.bt_5cm.Click += new System.EventHandler(this.bt_5cm_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(21, 145);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(278, 317);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // bt_connect
            // 
            this.bt_connect.Location = new System.Drawing.Point(21, 70);
            this.bt_connect.Name = "bt_connect";
            this.bt_connect.Size = new System.Drawing.Size(121, 23);
            this.bt_connect.TabIndex = 1;
            this.bt_connect.Text = "開始";
            this.bt_connect.UseVisualStyleBackColor = true;
            this.bt_connect.Click += new System.EventHandler(this.bt_connect_Click);
            // 
            // cb_com
            // 
            this.cb_com.FormattingEnabled = true;
            this.cb_com.Location = new System.Drawing.Point(21, 31);
            this.cb_com.Name = "cb_com";
            this.cb_com.Size = new System.Drawing.Size(121, 20);
            this.cb_com.TabIndex = 0;
            this.cb_com.SelectedIndexChanged += new System.EventHandler(this.cb_com_SelectedIndexChanged);
            // 
            // timGetData
            // 
            this.timGetData.Tick += new System.EventHandler(this.timGetData_Tick);
            // 
            // groupBox_debug
            // 
            this.groupBox_debug.Controls.Add(this.label_CH0value);
            this.groupBox_debug.Controls.Add(this.lb_save_offset);
            this.groupBox_debug.Controls.Add(this.lb_save_delta);
            this.groupBox_debug.Controls.Add(this.lb_save_cap_5cm);
            this.groupBox_debug.Controls.Add(this.lb_save_cap_10cm);
            this.groupBox_debug.Controls.Add(this.label2);
            this.groupBox_debug.Controls.Add(this.label3);
            this.groupBox_debug.Controls.Add(this.label4);
            this.groupBox_debug.Controls.Add(this.label5);
            this.groupBox_debug.Controls.Add(this.label6);
            this.groupBox_debug.Location = new System.Drawing.Point(215, 18);
            this.groupBox_debug.Name = "groupBox_debug";
            this.groupBox_debug.Size = new System.Drawing.Size(337, 120);
            this.groupBox_debug.TabIndex = 15;
            this.groupBox_debug.TabStop = false;
            this.groupBox_debug.Text = "Debug";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "保存容量(10cm)＝";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "保存容量(5cm)＝";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 12);
            this.label4.TabIndex = 13;
            this.label4.Text = "2点補正傾き＝";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "2点補正オフセット＝";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 15;
            this.label2.Text = "現在地＝";
            // 
            // lb_save_cap_10cm
            // 
            this.lb_save_cap_10cm.AutoSize = true;
            this.lb_save_cap_10cm.Location = new System.Drawing.Point(149, 96);
            this.lb_save_cap_10cm.Name = "lb_save_cap_10cm";
            this.lb_save_cap_10cm.Size = new System.Drawing.Size(95, 12);
            this.lb_save_cap_10cm.TabIndex = 16;
            this.lb_save_cap_10cm.Text = "lb_save_cap_10cm";
            // 
            // lb_save_cap_5cm
            // 
            this.lb_save_cap_5cm.AutoSize = true;
            this.lb_save_cap_5cm.Location = new System.Drawing.Point(136, 74);
            this.lb_save_cap_5cm.Name = "lb_save_cap_5cm";
            this.lb_save_cap_5cm.Size = new System.Drawing.Size(89, 12);
            this.lb_save_cap_5cm.TabIndex = 17;
            this.lb_save_cap_5cm.Text = "lb_save_cap_5cm";
            // 
            // lb_save_delta
            // 
            this.lb_save_delta.AutoSize = true;
            this.lb_save_delta.Location = new System.Drawing.Point(154, 56);
            this.lb_save_delta.Name = "lb_save_delta";
            this.lb_save_delta.Size = new System.Drawing.Size(71, 12);
            this.lb_save_delta.TabIndex = 18;
            this.lb_save_delta.Text = "lb_save_delta";
            // 
            // lb_save_offset
            // 
            this.lb_save_offset.AutoSize = true;
            this.lb_save_offset.Location = new System.Drawing.Point(149, 41);
            this.lb_save_offset.Name = "lb_save_offset";
            this.lb_save_offset.Size = new System.Drawing.Size(76, 12);
            this.lb_save_offset.TabIndex = 19;
            this.lb_save_offset.Text = "lb_save_offset";
            // 
            // label_CH0value
            // 
            this.label_CH0value.AutoSize = true;
            this.label_CH0value.Location = new System.Drawing.Point(190, 20);
            this.label_CH0value.Name = "label_CH0value";
            this.label_CH0value.Size = new System.Drawing.Size(35, 12);
            this.label_CH0value.TabIndex = 20;
            this.label_CH0value.Text = "label3";
            // 
            // fm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 507);
            this.Controls.Add(this.groupBox1);
            this.Name = "fm_main";
            this.Text = "水位計測";
            this.groupBox1.ResumeLayout(false);
            this.group_water.ResumeLayout(false);
            this.group_water.PerformLayout();
            this.group_calib.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox_debug.ResumeLayout(false);
            this.groupBox_debug.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bt_connect;
        private System.Windows.Forms.ComboBox cb_com;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timGetData;
        private System.Windows.Forms.GroupBox group_calib;
        private System.Windows.Forms.Button bt_10cm;
        private System.Windows.Forms.Button bt_5cm;
        private System.Windows.Forms.GroupBox group_water;
        private System.Windows.Forms.Label label_water;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bt_calreset;
        private System.Windows.Forms.GroupBox groupBox_debug;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_CH0value;
        private System.Windows.Forms.Label lb_save_offset;
        private System.Windows.Forms.Label lb_save_delta;
        private System.Windows.Forms.Label lb_save_cap_5cm;
        private System.Windows.Forms.Label lb_save_cap_10cm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

