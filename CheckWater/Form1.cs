﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Xml.Linq;

namespace CheckWater
{
    public partial class fm_main : Form
    {
        const string APP_VERSION = "Ver 1.11";
        static SerialPort sp;
        static double capdata_5cm;
        static double capdata_10cm;
        static double delta;
        static double calib_offset;
        static XElement default_setting_data;
        String setting_file_path = @"setting.xml";

        public fm_main(params string[] arg)
        {
            InitializeComponent();
            this.Text = "水位計測" + " " + APP_VERSION;

            // List ComPort and Select 1st item
            pmListCom();
            if (cb_com.Items.Count >= 1)
            {
                cb_com.SelectedIndex = 0;
            }

            // 設定ファイルのロード
            XDocument xml = XDocument.Load(setting_file_path);
            default_setting_data = xml.Element("FDC2214DATA");

            // Initial Variable
            SetInitValue();
            SetDebugModeOff();
            // デバッグモードの時は、CH0の取得値ラベルを表示する
            try
            {
                if (arg[0] == "debug")
                {
                    SetDebugModeOn();
                }
            }catch { }

        }

        private void cb_com_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void bt_connect_Click(object sender, EventArgs e)
        {
            if (bt_connect.Text == "開始")
            {
                try
                {
                    sp = new SerialPort();
                    sp.PortName = cb_com.SelectedItem.ToString();
                    sp.BaudRate = 115200;
                    sp.DataBits = 8;
                    sp.Parity = Parity.None;
                    sp.StopBits = StopBits.One;
                    sp.Encoding = Encoding.ASCII;

                    sp.Open();
                    bt_connect.Text = "停止";
                    timGetData.Start();
                    group_calib.Enabled = true;
                }
                catch
                {
                    sp.Close();
                    sp.Dispose();
                    timGetData.Stop();
                    bt_connect.Text = "開始";
                }
            }
            else
            {
                timGetData.Stop();
                sp.Close();
                sp.Dispose();
                bt_connect.Text = "開始";
            }
        }

        // PRIVATE
        private void pmListCom()
        {
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                this.cb_com.Items.Add(port);
            }
        }

        private void timGetData_Tick(object sender, EventArgs e)
        {
            try{
                // シリアルデータを取得
                string spdata = sp.ReadExisting();
                long[] ch_data = SplitRawdata(spdata);

                // 生データラベルの更新
                if (ch_data[0] != 0){
                    UpdateDebugLabel(ch_data[0]);

                    // 現在の水位ラベルの更新
                    if (group_calib.Text == "キャリブレーション(済)")
                    {
                        double wp = cal_water_position(ch_data[0]);
                        label_water.Text = wp.ToString("F2");
                    }
                    else
                    {
                        label_water.Text = "未設定";
                    }
                }
            }
            catch
            {
                // 何かしらの理由によって、シリアルポートが切断された場合
                timGetData.Stop();
                sp.Close();
                sp.Dispose();
                MessageBox.Show("シリアルポートが切断されました。\n再接続してください");
                bt_connect.Text = "開始";
            }
        }

        private long[] SplitRawdata(string str)
        {
            string rawdata_ch0 = "";
            string rawdata_ch1 = "";
            long[] ch = new long[2];

            if (str.IndexOf("Receive Data(") >= 0)
            {
                str = str.Substring(str.IndexOf("Receive Data("));
                try
                {
                    rawdata_ch0 = str.Substring(13, 8);
                    rawdata_ch1 = str.Substring(22, 8);
                }
                catch
                {
                    rawdata_ch0 = "0";
                    rawdata_ch1 = "0";
                }

                try
                {
                    ch[0] = Convert.ToInt64(rawdata_ch0, 16);
                    ch[1] = Convert.ToInt64(rawdata_ch1, 16);
                }
                catch
                {
                    ch[0] = 0;
                    ch[1] = 0;
                }
            }
            return ch;
        }
        private void bt_5cm_Click(object sender, EventArgs e)
        {
            capdata_5cm = Convert.ToDouble(label_CH0value.Text);
            reload_group_calib();   // 戻り値いらない
            bt_5cm.Text = "5cm水位を補正(済)";
            default_setting_data.Element("calib_5cm").Element("datas").Value = capdata_5cm.ToString();
            default_setting_data.Save(setting_file_path);
        }
        private void bt_10cm_Click(object sender, EventArgs e)
        {
            capdata_10cm = Convert.ToDouble(label_CH0value.Text);
            reload_group_calib();   // 戻り値いらない
            bt_10cm.Text = "10cm水位を補正(済)";
            default_setting_data.Element("calib_10cm").Element("datas").Value = capdata_10cm.ToString();
            default_setting_data.Save(setting_file_path);
        }
        // キャリブレーション設定が完了していれば戻り値true
        private bool reload_group_calib()
        {
            bool ret = false;
            if(capdata_5cm!=0 && capdata_10cm != 0)
            {
                group_calib.Text = "キャリブレーション(済)";
                bt_5cm.Text = "5cm水位を補正(済)";
                bt_10cm.Text = "10cm水位を補正(済)";
                delta = (capdata_10cm - capdata_5cm) / 5.00;
                ret = true;
            }else
            {
                group_calib.Text = "キャリブレーション(未設定)";
                delta = (capdata_10cm - capdata_5cm) / 5.00;
            }

            if (delta != 0)
            {
                calib_offset = capdata_5cm - (delta * 5);
            }

            return ret;
        }
        private double cal_water_position(long nowdata)
        {
            double ret = 0;
            ret = (nowdata-calib_offset) / delta;
            return ret;
        }
        private void SetInitValue()
        {
            var ini_5cm = default_setting_data.Element("calib_5cm").Element("datas").Value;
            double initdata = Convert.ToDouble(ini_5cm);
            if (initdata != 0)
            {
                capdata_5cm = initdata;
            }
            else
            {
                capdata_5cm = 0;
            }
            var ini_10cm = default_setting_data.Element("calib_10cm").Element("datas").Value;
            initdata = Convert.ToDouble(ini_10cm);
            if (initdata != 0)
            {
                capdata_10cm = initdata;
            }
            else
            {
                capdata_10cm = 0;
            }
            delta = 0;
            calib_offset = 0;
            //group_calib.Enabled = false;
            reload_group_calib();
        }

        private void bt_calreset_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("キャリブレーション情報をリセットしますか?","キャリブレーション", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                default_setting_data.Element("calib_5cm").Element("datas").Value = "0";
                default_setting_data.Element("calib_10cm").Element("datas").Value = "0";
                SetInitValue();
                default_setting_data.Save(setting_file_path);

                bt_5cm.Enabled = true;
                bt_10cm.Enabled = true;
            }

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void SetDebugModeOn()
        {
            groupBox_debug.Visible = true;
        }

        private void SetDebugModeOff()
        {
            groupBox_debug.Visible = false;
        }

        private void UpdateDebugLabel(long cap)
        {
            label_CH0value.Text = Convert.ToString(cap);
            label_CH0value.Update();

            lb_save_offset.Text = Convert.ToString(calib_offset);
            lb_save_offset.Update();

            lb_save_delta.Text = Convert.ToString(delta);
            lb_save_delta.Update();

            lb_save_cap_5cm.Text = Convert.ToString(capdata_5cm);
            lb_save_cap_5cm.Update();

            lb_save_cap_10cm.Text = Convert.ToString(capdata_10cm);
            lb_save_cap_10cm.Update();
        }

    }
}
