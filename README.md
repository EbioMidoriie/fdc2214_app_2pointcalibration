# README #
水位計測用アプリケーション

* Quick summary
ESEL LORAモジュール用 + FDC2214ノードの水位情報取得アプリケーションです。

* Version
1.00：新規作成
1.10：2点キャリブレーション機能を追加

* Summary of set up
Windows用です。
プロジェクトをダウンロードして/CheckWater/bin/debug下のCheckWater.exeを直接起動させて下さい。

* Configuration
.net framework v4.6.1を使用しています。
アプリケーションが起動しない場合は、以下から.net frameworkをインストールして下さい。
https://www.microsoft.com/ja-jp/download/details.aspx?id=49981

